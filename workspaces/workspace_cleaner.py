"""Remove workspace data produced by completed pipelines to avoid filling the disk.

Workspace data is organised as directories named along pipeline id.
These directories are located in the so-called workspace base directory.

Note: pipelines belong to different projects found in open-mobility-indicators/indicators group

Keep the data directories corresponding to the N latest failed and the M latest successful pipelines,
delete everything else (N and M being options) except workspaces related to running pipelines.
"""

import logging
import shutil
import time
from itertools import islice
from operator import attrgetter
from pathlib import Path
from typing import Iterator

import daiquiri
import gitlab
import typer
from gitlab.v4.objects import Project, ProjectPipeline

logger = logging.getLogger(__name__)


def main(
    workspace_base_dir: Path = typer.Option(
        ..., envvar="WORKSPACE_BASE_DIR", help="Workspace base directory"
    ),
    gitlab_url: str = typer.Option(
        ..., envvar="GITLAB_URL", help="Base URL of GitLab instance"
    ),
    gitlab_group_id: int = typer.Option(
        ...,
        envvar="GITLAB_GROUP_ID",
        help="consider pipelines triggered by projects of this group",
    ),
    gitlab_private_token: str = typer.Option(
        ...,
        envvar="GITLAB_PRIVATE_TOKEN",
        help="Private access token used to authenticate to GitLab API",
    ),
    debug: bool = typer.Option(
        False, "-d", "--debug", envvar="DEBUG", help="display debug logging messages"
    ),
    dry_run: bool = typer.Option(
        False,
        "-n",
        "--dry-run",
        envvar="DRY_RUN",
        help="do not really delete directories",
    ),
    verbose: bool = typer.Option(
        False, "-v", "--verbose", envvar="VERBOSE", help="display info logging messages"
    ),
    keep_failed: int = typer.Option(
        3,
        "--keep_max_failed",
        envvar="KEEP_MAX_FAILED",
        help="keep this number of failed pipeline directories in workspace",
    ),
    keep_success: int = typer.Option(
        1,
        "--keep_max_success",
        envvar="KEEP_MAX_SUCCESS",
        help="keep this number of success pipeline directories in workspace",
    ),
):
    script_start_time = time.time()

    daiquiri.setup()
    logger.setLevel(
        logging.DEBUG if debug else logging.INFO if verbose else logging.WARNING
    )

    # Authentication
    gitlab_url = gitlab_url.rstrip("/")
    gl = gitlab.Gitlab(gitlab_url, private_token=gitlab_private_token, api_version="4")
    if gitlab_private_token is not None:
        gl.auth()

    logger.debug("Retrieving group from group id %r", gitlab_group_id)
    project_group = gl.groups.get(gitlab_group_id)
    if project_group is None:
        typer.exit(f"Can't retrieve group #{gitlab_group_id}")

    logger.debug("Retrieving projects from parent group")
    project_id_set = {
        project.id for project in project_group.projects.list(as_list=False)
    }

    logger.debug("Retrieving last pipelines from projects")
    keep_pipeline_ids: dict[str, list[int]] = retrieve_last_pipelines_from_project_ids(
        gl, project_id_set, keep_failed=keep_failed, keep_success=keep_success
    )

    logger.debug("Keeping pipeline IDs %r", keep_pipeline_ids)

    for workspace_dir in sorted(workspace_base_dir.iterdir()):
        if not workspace_dir.is_dir():
            logger.debug(
                "Ignoring file %r in workspace base directory", str(workspace_dir)
            )
            continue

        try:
            pipeline_id = int(workspace_dir.name)
        except ValueError:
            logger.debug(
                "Ignoring sub-directory %r of workspace directory that does not seem to be a pipeline ID",
                str(workspace_dir),
            )
            continue

        if workspace_dir.stat().st_ctime > script_start_time:
            logger.debug(
                "Ignoring workspace directory %r as it has been created since script started",
                str(workspace_dir),
            )
            continue

        if pipeline_id in keep_pipeline_ids["running"]:
            logger.debug(
                "Keeping workspace directory %r, associated pipeline is a running pipeline",
                str(workspace_dir),
            )
            continue

        if pipeline_id in keep_pipeline_ids["success"]:
            logger.debug(
                "Keeping workspace directory %r, associated pipeline is one of the successful pipelines to keep",
                str(workspace_dir),
            )
            continue

        if pipeline_id in keep_pipeline_ids["failed"]:
            logger.debug(
                "Keeping workspace directory %r, associated pipeline is one of the failed pipelines to keep",
                str(workspace_dir),
            )
            continue

        logger.info(
            "Deleting outdated workspace directory %r...",
            str(workspace_dir),
        )
        if not dry_run:
            shutil.rmtree(workspace_dir)


def retrieve_last_pipelines_from_project_ids(
    gl, project_ids: set, *, keep_failed: int, keep_success: int
) -> dict[str, list[int]]:
    def sort_and_slice(pipeline_list: list, keep: int) -> list:
        sorted_pipelines = sorted(
            pipeline_list, key=attrgetter("updated_at"), reverse=True
        )
        return sorted_pipelines[:keep]

    pipelines: dict[str, list] = {"failed": [], "running": [], "success": []}

    # Enrich pipelines with last pipelines from projects
    for project_id in project_ids:
        project = gl.projects.get(project_id)
        logger.debug("- %s", project.name)

        project_pipelines = retrieve_last_pipelines_by_project(
            project, keep_failed=keep_failed, keep_success=keep_success
        )
        for k in pipelines:
            pipelines[k].extend(project_pipelines[k])

    # Reduce success and failed pipelines to the number we want
    # Running pipelines are all kept
    pipelines["failed"] = sort_and_slice(pipelines["failed"], keep_failed)
    pipelines["success"] = sort_and_slice(pipelines["success"], keep_success)

    # Turn pipeline lists into pipeline id list
    return {k: [pipeline.id for pipeline in v] for k, v in pipelines.items()}


def retrieve_last_pipelines_by_project(
    project, *, keep_failed: int, keep_success: int
) -> dict[str, list]:
    return {
        "failed": list(
            islice(get_latest_pipelines(project, status="failed"), keep_failed)
        ),
        "success": list(
            islice(get_latest_pipelines(project, status="success"), keep_success)
        ),
        "running": list(get_latest_pipelines(project, status="running")),
    }


def get_latest_pipelines(project: Project, *, status: str) -> Iterator[ProjectPipeline]:
    return project.pipelines.list(
        status=status, order_by="updated_at", sorted="desc", as_list=False
    )


if __name__ == "__main__":
    typer.run(main)
